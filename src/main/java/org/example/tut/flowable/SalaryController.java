package org.example.tut.flowable;

import java.util.List;

import org.example.tut.flowable.dto.ProcessInstanceResponse;
import org.example.tut.flowable.dto.SalaryRequest;
import org.example.tut.flowable.dto.TaskDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class SalaryController {

	SalaryService salaryService;

	// **********************************************************
	@PostMapping("/salary/apply")
	public ProcessInstanceResponse applySalary(@RequestBody SalaryRequest salaryRequest) {
		return salaryService.applySalaryRequest(salaryRequest);
	}

	@GetMapping("/manager/tasks")
	public List<TaskDetails> getTasks() {
		return salaryService.getManagerTasks();
	}

	@PostMapping("/manager/approve/tasks/{taskId}/{approved}")
	public void approveTask(@PathVariable("taskId") String taskId, @PathVariable("approved") Boolean approved) {
		salaryService.approveSalary(taskId, approved);
	}

	@GetMapping("/user/tasks")
	public List<TaskDetails> getUserTasks() {
		return salaryService.getUserTasks();
	}

	@PostMapping("/user/accept/{taskId}")
	public void acceptSalary(@PathVariable("taskId") String taskId) {
		salaryService.acceptSalary(taskId);
	}
	
	
	// ********************************************************** deployment
	// endpoints **********************************************************
	@PostMapping("/deploy")
	public void deployWorkflow() {
		salaryService.deployProcessDefinition();
	}

	@GetMapping("/process/{processId}")
	public void checkState(@PathVariable("processId") String processId) {
		salaryService.checkProcessHistory(processId);
	}

}
