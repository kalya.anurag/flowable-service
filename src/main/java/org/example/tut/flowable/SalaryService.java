package org.example.tut.flowable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.example.tut.flowable.dto.ProcessInstanceResponse;
import org.example.tut.flowable.dto.SalaryRequest;
import org.example.tut.flowable.dto.TaskDetails;
import org.flowable.engine.HistoryService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SalaryService {

	public static final String TASK_CANDIDATE_GROUP = "managers";
	public static final String PROCESS_DEFINITION_KEY = "salaryRequest";
	public static final String DDO_ID = "DDO1";
	public static final String REJECT_NAME = "Salary Rejected";
	RuntimeService runtimeService;
	TaskService taskService;
	ProcessEngine processEngine;
	RepositoryService repositoryService;

	// ********************************************************** deployment service
	// methods **********************************************************

	public void deployProcessDefinition() {

		Deployment deployment = repositoryService.createDeployment().addClasspathResource("salary-request.bpmn20.xml")
				.deploy();

	}

	// ********************************************************** process service
	// methods **********************************************************

	public ProcessInstanceResponse applySalaryRequest(SalaryRequest salaryRequest) {

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("ddoid", salaryRequest.getDdoId());
		variables.put("employeeCode", salaryRequest.getEmpCode());
		variables.put("employeeName", salaryRequest.getEmpName());
		variables.put("employeeGrade", salaryRequest.getEmpGrade());
		variables.put("lastDrawnSalary", salaryRequest.getLastDrawnSalary());
		variables.put("description", salaryRequest.getRequestDescription());

		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);

		return new ProcessInstanceResponse(processInstance.getId(), processInstance.isEnded());
	}

	public List<TaskDetails> getManagerTasks() {
		List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup(TASK_CANDIDATE_GROUP).list();
		List<TaskDetails> taskDetails = getTaskDetails(tasks);

		return taskDetails;
	}

	private List<TaskDetails> getTaskDetails(List<Task> tasks) {
		List<TaskDetails> taskDetails = new ArrayList<>();
		for (Task task : tasks) {
			Map<String, Object> processVariables = taskService.getVariables(task.getId());
			taskDetails.add(new TaskDetails(task.getId(), task.getName(), processVariables));
		}
		return taskDetails;
	}

	public void approveSalary(String taskId, Boolean approved) {

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("approved", approved.booleanValue());
		taskService.complete(taskId, variables);
	}

	public void acceptSalary(String taskId) {
		taskService.complete(taskId);
	}

	public List<TaskDetails> getUserTasks() {

		List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup(TASK_CANDIDATE_GROUP)
				.list();/* taskCandidateOrAssigned(DDO_ID).list(); */
		List<Task> tasks1 = taskService.createTaskQuery().taskCandidateOrAssigned(DDO_ID).list();
		List<Task> tasks3 = taskService.createTaskQuery().taskName(REJECT_NAME).list();
		tasks.addAll(tasks1);
		tasks.addAll(tasks3);
		String id;
		List<TaskDetails> tasks2 = new ArrayList<TaskDetails>();
		// taskService.createTaskQuery().taskCandidateGroup(TASK_CANDIDATE_GROUP).list();
		List<TaskDetails> taskDetails = getTaskDetails(tasks);
		for (TaskDetails task : taskDetails) {
			id = (String) task.getTaskData().get("ddoid");
			if (id.equalsIgnoreCase(DDO_ID)) {
				tasks2.add(task);
			}
		}
		return tasks2;
	}

	public void checkProcessHistory(String processId) {

		HistoryService historyService = processEngine.getHistoryService();

		List<HistoricActivityInstance> activities = historyService.createHistoricActivityInstanceQuery()
				.processInstanceId(processId).finished().orderByHistoricActivityInstanceEndTime().asc().list();

		for (HistoricActivityInstance activity : activities) {
			System.out.println(activity.getActivityId() + " took " + activity.getDurationInMillis() + " milliseconds");
		}

		System.out.println("\n \n \n \n");
	}

}
