package org.example.tut.flowable.dto;

import lombok.Data;

@Data
public class SalaryRequest {

	String ddoId;
	String empCode;
	String empName;
	String empGrade;
	String lastDrawnSalary;
	String requestDescription;

}